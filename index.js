const request = require('request')
var async = require('async');

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://jaskeera:jaskeerat4@ds145093.mlab.com:45093/wholesome';

// Database Name
const dbName = 'wholesome';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to mongo server");

    const db = client.db(dbName);
    op(db);
    //test the function
    //ingredientsCompute(db, {}, "https://cooking.nytimes.com/recipes/1019323-fish-fry-chepa-vepudu", "https://cooking.nytimes.com/recipes/1019323-fish-fry-chepa-vepudu" , function () {});
});


function op(db) {
    var cat = "American";
    request.get({
        url: "https://api.nytimes.com/svc/search/v2/articlesearch.json",
        qs: {
            'api-key': "f0eef7511ade426aa21a9b456000278d",
            'q': cat,
            'fq': "type_of_material:(\"Recipe\")"
        },
    }, function (err, response, body) {
        body = JSON.parse(body);
        var docs = body.response.docs;

        var asyncArr = [];
        for (var i=0; ((i<5) && (i<docs.length)); i++) {
            var index = i;
            var doc = docs[index];
            const url = doc.web_url;
            const image = "https://static01.nyt.com/" + doc.multimedia[doc.multimedia.length - 1].url;

            asyncArr[index] = ingredientsCompute.bind(null, db, cat,  doc, url, image);
        }

        async.series(asyncArr, function (error, results) {
            if (error) {
                errorResponse("Error: " + error);
            }
            else {
                successReponse(results);
            }
        });
    });
}


const ingredientsCompute = function(db, category,  doc, url, image, callback) {
    //call parser
    var obj = {
        url : url,
        image: image,
	category: category
    };
    //console.log(obj);
    //callback(null, "ok");
    getParsedData(db, url, obj, callback);
}

function writeFile(filename, contents) {
    var fs = require('fs');
    var stream = fs.createWriteStream(filename);
    stream.once('open', function(fd) {
        stream.write(contents);
        stream.end();
    });
}

function getParsedData(db, url, obj, callback) {

    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["Scraper/scrape.py", url]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        // callback(data.toString());
        data = data.toString();
        var arr = data.split(/\r?\n/);
        var blanks = 0;
        var ingredients = "";
        var title = arr[0];
        obj.title = title;
        for(var j=1; j<arr.length; j++) {
            if(arr[j] === "") {
                blanks++;
            }
            if(blanks == 2) {
                ingredients = ingredients + arr[j] + "\n";
            }
        }
        writeFile("inter1.txt", ingredients);
        try {
            getIngredientsJSON(ingredients, function (data) {
                obj.ingredients = JSON.parse(data.toString());
                console.log(obj);
                addToDb(db, obj, "recipe");

            });
        }
        finally {
            console.log(obj);
            callback(null, "ok");
        }
    });
}


function getIngredientsJSON(str, callback) {
    stage1(str, function (data1) {
        stage2(data1, function (result) {
            callback(result);
        });
    });
}

function stage1(str, callback) {
    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["/ingredient-phrase-tagger/bin/parse-ingredients.py", "inter1.txt"]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        writeFile("inter2.txt", data.toString());
        callback(data.toString());
    });
}

function stage2(str, callback) {
    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["/ingredient-phrase-tagger/bin/convert-to-json.py", "inter2.txt"]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        callback(data);
    });
}


function addToDb(db, data, task) {
    const collection = db.collection(task);
    // Insert some documents

    collection.insertOne(data, function(err, result) {
        if(err)
            console.log("Error: " + err + " inserting. Task: " + task);
        else
            console.log("Inserted data into mongo | Task: " + task);
    });
}


function successReponse(result) {
    sendResponse(1, result)
}

function errorResponse(error) {
    console.log(error);
    sendResponse(0, {})
}

function sendResponse(status, result) {
    console.log(result);
}

